TEMPLATE = app
CONFIG += c++11 console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += demo0/demo0.cpp

HEADERS += \
demo0/font3x5.h \
demo0/font5x7.h \
demo0/zarduboy.h \
demo0/zmain.h \
demo0/zfw.h

win32 {
QMAKE_CXXFLAGS_WARN_ON -= -w34100
INCLUDEPATH += SDL/include
contains(QMAKE_TARGET.arch, x86_64) {
LIBS += -L$$PWD/SDL/lib/x64/
} else {
LIBS += -L$$PWD/SDL/lib/x86/
}
LIBS += -lSDL2 -lSDL2main
}

linux {
LIBS += -lSDL2 -lSDL2main
}
