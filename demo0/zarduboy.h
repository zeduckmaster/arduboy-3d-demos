#pragma once

#include "zfw.h"

#if defined(ARDUINO_ARCH_AVR)

#include <Arduino.h>
#include <SPI.h>

#if !defined(cbi)
	#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#if !defined(sbi)
	#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

extern int __heap_start;
extern int* __brkval;

#if defined(ARDUINO_AVR_ARDUBOY)
#if !defined(ARDUBOY_10)
#define ARDUBOY_10
#endif
#endif

#elif defined(_WIN32) || defined(__linux__)

#if defined(_WIN32)
#include <SDL.h>
#elif defined(__linux__)
#include <SDL2/SDL.h>
#endif
#include <string>
#include <iostream>
#include <chrono>

inline void delay(uint32_t ms) { SDL_Delay(ms); }

auto const kStartTime = std::chrono::high_resolution_clock::now();

inline uint32_t millis()
{
	auto curTime = std::chrono::high_resolution_clock::now();
	return static_cast<uint32_t>(std::chrono::duration_cast<std::chrono::milliseconds>(curTime - kStartTime).count());
}

auto sdlQuit = false;

struct _Serial
{
	void begin(uint32_t baud) {}
	template<typename T>
	inline void print(T const& v) { std::cout << v; }
	template<typename T>
	inline void println(T const& v) { std::cout << v << std::endl; }
	inline void flush() {}
};
_Serial Serial;

#endif

namespace z
{
#if defined(ARDUBOY_10)

	uint8_t constexpr kPinToBit[] =
	{
		_BV(2), // D0 - PD2
		_BV(3),	// D1 - PD3
		_BV(1), // D2 - PD1
		_BV(0),	// D3 - PD0
		_BV(4),	// D4 - PD4
		_BV(6), // D5 - PC6
		_BV(7), // D6 - PD7
		_BV(6), // D7 - PE6

		_BV(4), // D8 - PB4
		_BV(5),	// D9 - PB5
		_BV(6), // D10 - PB6
		_BV(7),	// D11 - PB7
		_BV(6), // D12 - PD6
		_BV(7), // D13 - PC7

		_BV(3),	// D14 - MISO - PB3
		_BV(1),	// D15 - SCK - PB1
		_BV(2),	// D16 - MOSI - PB2
		_BV(0),	// D17 - SS - PB0

		_BV(7),	// D18 - A0 - PF7
		_BV(6), // D19 - A1 - PF6
		_BV(5), // D20 - A2 - PF5
		_BV(4), // D21 - A3 - PF4
		_BV(1), // D22 - A4 - PF1
		_BV(0), // D23 - A5 - PF0

		_BV(4), // D24 / D4 - A6 - PD4
		_BV(7), // D25 / D6 - A7 - PD7
		_BV(4), // D26 / D8 - A8 - PB4
		_BV(5), // D27 / D9 - A9 - PB5
		_BV(6), // D28 / D10 - A10 - PB6
		_BV(6), // D29 / D12 - A11 - PD6
		_BV(5)  // D30 / TX Led - PD5
	};

	uint8_t constexpr kPinToPort[] =
	{
		4, // D0 - PD2
		4, // D1 - PD3
		4, // D2 - PD1
		4, // D3 - PD0
		4, // D4 - PD4
		3, // D5 - PC6
		4, // D6 - PD7
		5, // D7 - PE6

		2, // D8 - PB4
		2, // D9 - PB5
		2, // D10 - PB6
		2, // D11 - PB7
		4, // D12 - PD6
		3, // D13 - PC7

		2, // D14 - MISO - PB3
		2, // D15 - SCK - PB1
		2, // D16 - MOSI - PB2
		2, // D17 - SS - PB0

		6, // D18 - A0 - PF7
		6, // D19 - A1 - PF6
		6, // D20 - A2 - PF5
		6, // D21 - A3 - PF4
		6, // D22 - A4 - PF1
		6, // D23 - A5 - PF0

		4, // D24 / D4 - A6 - PD4
		4, // D25 / D6 - A7 - PD7
		2, // D26 / D8 - A8 - PB4
		2, // D27 / D9 - A9 - PB5
		2, // D28 / D10 - A10 - PB6
		4, // D29 / D12 - A11 - PD6
		4  // D30 / TX Led - PD5
	};

	uint8_t constexpr kPortToMode[] =
	{
		0,
		0,
		0x24, // DDRB
		0x27, // DDRC
		0x2A, // DDRD
		0x2D, // DDRE
		0x30  // DDRF
	};

	uint8_t constexpr kPortToOutput[] =
	{
		0,
		0,
		0x25, // PORTB
		0x28, // PORTC
		0x2B, // PORTD
		0x2E, // PORTE
		0x31  // PORTF
	};

	uint8_t constexpr kPortToInput[] =
	{
		0,
		0,
		0x23, // PINB
		0x26, // PINC
		0x29, // PIND
		0x2C, // PINE
		0x2F  // PINF
	};

	uint8_t constexpr kPinToTimer[] =
	{
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		TIMER0B,		// 3
		NOT_ON_TIMER,
		TIMER3A,		// 5
		TIMER4D,		// 6
		NOT_ON_TIMER,

		NOT_ON_TIMER,
		TIMER1A,		// 9
		TIMER1B,		// 10
		TIMER0A,		// 11

		NOT_ON_TIMER,
		TIMER4A,		// 13

		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,

		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
		NOT_ON_TIMER,
	};

	finline void turnOffPWM(uint8_t timer)
	{
		switch(timer)
		{
		#if defined(TCCR1A) && defined(COM1A1)
		case TIMER1A:   cbi(TCCR1A, COM1A1);    break;
		#endif
		#if defined(TCCR1A) && defined(COM1B1)
		case TIMER1B:   cbi(TCCR1A, COM1B1);    break;
		#endif
		#if defined(TCCR1A) && defined(COM1C1)
		case TIMER1C:   cbi(TCCR1A, COM1C1);    break;
		#endif

		#if defined(TCCR2) && defined(COM21)
		case  TIMER2:   cbi(TCCR2, COM21);      break;
		#endif

		#if defined(TCCR0A) && defined(COM0A1)
		case  TIMER0A:  cbi(TCCR0A, COM0A1);    break;
		#endif

		#if defined(TCCR0A) && defined(COM0B1)
		case  TIMER0B:  cbi(TCCR0A, COM0B1);    break;
		#endif
		#if defined(TCCR2A) && defined(COM2A1)
		case  TIMER2A:  cbi(TCCR2A, COM2A1);    break;
		#endif
		#if defined(TCCR2A) && defined(COM2B1)
		case  TIMER2B:  cbi(TCCR2A, COM2B1);    break;
		#endif

		#if defined(TCCR3A) && defined(COM3A1)
		case  TIMER3A:  cbi(TCCR3A, COM3A1);    break;
		#endif
		#if defined(TCCR3A) && defined(COM3B1)
		case  TIMER3B:  cbi(TCCR3A, COM3B1);    break;
		#endif
		#if defined(TCCR3A) && defined(COM3C1)
		case  TIMER3C:  cbi(TCCR3A, COM3C1);    break;
		#endif

		#if defined(TCCR4A) && defined(COM4A1)
		case  TIMER4A:  cbi(TCCR4A, COM4A1);    break;
		#endif
		#if defined(TCCR4A) && defined(COM4B1)
		case  TIMER4B:  cbi(TCCR4A, COM4B1);    break;
		#endif
		#if defined(TCCR4A) && defined(COM4C1)
		case  TIMER4C:  cbi(TCCR4A, COM4C1);    break;
		#endif
		#if defined(TCCR4C) && defined(COM4D1)
		case TIMER4D:	cbi(TCCR4C, COM4D1);	break;
		#endif

		#if defined(TCCR5A)
		case  TIMER5A:  cbi(TCCR5A, COM5A1);    break;
		case  TIMER5B:  cbi(TCCR5A, COM5B1);    break;
		case  TIMER5C:  cbi(TCCR5A, COM5C1);    break;
		#endif
		}
	}

	finline void pinModeOutput(uint8_t pin)
	{
		auto port = kPinToPort[pin];
		auto bit = kPinToBit[pin];
		auto reg = reinterpret_cast<volatile uint8_t*>(kPortToMode[port]);
		*reg |= bit;
	}

	void spiBegin()
	{
		auto sreg = SREG;
		noInterrupts(); // protect from a scheduler and prevent transactionBegin
		// set SS to high so a connected chip will be "deselected" by default
		// when the SS pin is set as OUTPUT, it can be used as
		// a general purpose output port (it doesn't influence
		// SPI operations).
		pinModeOutput(SS);

		// warning: if the SS pin ever becomes a LOW INPUT then SPI
		// automatically switches to Slave, so the data direction of
		// the SS pin MUST be kept as OUTPUT.
		SPCR |= _BV(MSTR);
		SPCR |= _BV(SPE);

		// set direction register for SCK and MOSI pin.
		// MISO pin automatically overrides to INPUT.
		// by doing this AFTER enabling SPI, we avoid accidentally
		// clocking in a single bit since the lines go directly
		// from "input" to SPI control.
		// http://code.google.com/p/arduino/issues/detail?id=888
		pinModeOutput(SCK);
		pinModeOutput(MOSI);

		SREG = sreg;
	}

	void spiTransfer(uint8_t data)
	{
		SPDR = data;
		asm volatile("nop");
		while(!(SPSR & _BV(SPIF))); // wait
	}

	finline void spiSetClockDivider(uint8_t clockDiv)
	{
		auto const kSPIClockMask = uint8_t{0x03}; // SPR1 = bit 1, SPR0 = bit 0 on SPCR
		auto const kSPI2XClockMask = uint8_t{0x01}; // SPI2X = bit 0 on SPSR
		SPCR = (SPCR & ~kSPIClockMask) | (clockDiv & kSPIClockMask);
		SPSR = (SPSR & ~kSPI2XClockMask) | ((clockDiv >> 2) & kSPI2XClockMask);
	}

	void spiSetDataMode(uint8_t dataMode)
	{
		auto kSPIModeMask = uint8_t{0x0C}; // CPOL = bit 3, CPHA = bit 2 on SPCR
		SPCR = (SPCR & ~kSPIModeMask) | dataMode;
	}

	finline void power_adc_disable() { (PRR0 |= (uint8_t)(1 << PRADC)); }

	finline void power_usart0_disable() { (PRR0 |= (uint8_t)(1 << PRUSART0)); }

	finline void power_twi_disable() { (PRR0 |= (uint8_t)(1 << PRTWI)); }

	finline void power_timer2_disable() { (PRR0 |= (uint8_t)(1 << PRTIM2)); }

	finline void power_usart1_disable() { (PRR1 |= (uint8_t)(1 << PRUSART1)); }

#endif

	uint8_t constexpr arduboy_logo[] PROGMEM =
	{
		0xF0, 0xF8, 0x9C, 0x8E, 0x87, 0x83, 0x87, 0x8E, 0x9C, 0xF8,
		0xF0, 0x00, 0x00, 0xFE, 0xFF, 0x03, 0x03, 0x03, 0x03, 0x03,
		0x07, 0x0E, 0xFC, 0xF8, 0x00, 0x00, 0xFE, 0xFF, 0x03, 0x03,
		0x03, 0x03, 0x03, 0x07, 0x0E, 0xFC, 0xF8, 0x00, 0x00, 0xFF,
		0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF,
		0x00, 0x00, 0xFE, 0xFF, 0x83, 0x83, 0x83, 0x83, 0x83, 0xC7,
		0xEE, 0x7C, 0x38, 0x00, 0x00, 0xF8, 0xFC, 0x0E, 0x07, 0x03,
		0x03, 0x03, 0x07, 0x0E, 0xFC, 0xF8, 0x00, 0x00, 0x3F, 0x7F,
		0xE0, 0xC0, 0x80, 0x80, 0xC0, 0xE0, 0x7F, 0x3F, 0xFF, 0xFF,
		0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0xFF, 0xFF, 0x00,
		0x00, 0xFF, 0xFF, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C, 0x3E, 0x77,
		0xE3, 0xC1, 0x00, 0x00, 0x7F, 0xFF, 0xC0, 0xC0, 0xC0, 0xC0,
		0xC0, 0xE0, 0x70, 0x3F, 0x1F, 0x00, 0x00, 0x1F, 0x3F, 0x70,
		0xE0, 0xC0, 0xC0, 0xC0, 0xE0, 0x70, 0x3F, 0x1F, 0x00, 0x00,
		0x7F, 0xFF, 0xC1, 0xC1, 0xC1, 0xC1, 0xC1, 0xE3, 0x77, 0x3E,
		0x1C, 0x00, 0x00, 0x1F, 0x3F, 0x70, 0xE0, 0xC0, 0xC0, 0xC0,
		0xE0, 0x70, 0x3F, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
		0xFF, 0xFF, 0x01, 0x00, 0x00, 0x00
	};
	auto constexpr arduboy_logo_width = uint8_t{88};
	auto constexpr arduboy_logo_height = uint8_t{16};

	enum eColor : uint8_t
	{
		Black = 0x00,
		White = 0xff
	};


	class Arduboy
	{
	public:
		static auto constexpr kInputUp = uint8_t{_BV(7)};
		static auto constexpr kInputRight = uint8_t{_BV(6)};
		static auto constexpr kInputDown = uint8_t{_BV(4)};
		static auto constexpr kInputLeft = uint8_t{_BV(5)};
		static auto constexpr kInputA = uint8_t{_BV(3)};
		static auto constexpr kInputB = uint8_t{_BV(2)};
		static auto constexpr kScreenWidth = int16_t{128};
		static auto constexpr kScreenHeight = int16_t{64};
		static auto constexpr kScreenRAMSize = int16_t{1024}; // 128*64/8
		static auto constexpr kDCPin = uint8_t{4};
		static auto constexpr kCSPin = uint8_t{12};
		static auto constexpr kRSTPin = uint8_t{6};

	#if defined(ARDUBOY_10)
	private:
		void _lcdCommandMode()
		{
			auto csOut = reinterpret_cast<volatile uint8_t*>(kPortToOutput[kPinToPort[kCSPin]]);
			auto csBit = kPinToBit[kCSPin];
			auto dcOut = reinterpret_cast<volatile uint8_t*>(kPortToOutput[kPinToPort[kDCPin]]);
			auto dcBit = kPinToBit[kDCPin];
			*csOut |= csBit; // CS HIGH
			*dcOut &= ~dcBit; // DC LOW
			*csOut &= ~csBit; // CS LOW
		}

		void _lcdDataMode()
		{
			auto csOut = reinterpret_cast<volatile uint8_t*>(kPortToOutput[kPinToPort[kCSPin]]);
			auto csBit = kPinToBit[kCSPin];
			auto dcOut = reinterpret_cast<volatile uint8_t*>(kPortToOutput[kPinToPort[kDCPin]]);
			auto dcBit = kPinToBit[kDCPin];
			//*csOut |= csBit; // CS HIGH needed?
			*dcOut |= dcBit; // DC HIGH
			*csOut &= ~csBit; // CS LOW
		}

	#endif

	public:
		int16_t width() const { return kScreenWidth; }
		int16_t height() const { return kScreenHeight; }

		void begin()
		{
		#if defined(ARDUBOY_10)
			spiBegin();

			// pins boot - optimized
			{
				// up, right, left, down
				auto mode = reinterpret_cast<uint8_t*>(0x30); // DDRF
				auto out = reinterpret_cast<uint8_t*>(0x31); // PORTF
				auto bit = uint8_t{_BV(7)|_BV(6)|_BV(5)|_BV(4)}; // A0,A1,A2,A3
				*mode &= ~bit;
				*out |= bit;
				// A
				mode = reinterpret_cast<uint8_t*>(0x2D); // DDRE
				out = reinterpret_cast<uint8_t*>(0x2E); // PORTE
				bit = _BV(6); // D7
				*mode &= ~bit;
				*out |= bit;
				// B
				mode = reinterpret_cast<uint8_t*>(0x24); // DDRB
				out = reinterpret_cast<uint8_t*>(0x25); // PORTB
				bit = _BV(4); // D8
				*mode &= ~bit;
				*out |= bit;
				// DC, CS, RST
				mode = reinterpret_cast<uint8_t*>(0x2A); // DDRD
				out = reinterpret_cast<uint8_t*>(0x2B); // PORTD
				bit = _BV(4)|_BV(6)|_BV(7); // D4, D12, D6
				*mode |= bit;
				// LED_RED, LED_GREEN, LED_BLUE
				mode = reinterpret_cast<uint8_t*>(0x24); // DDRB
				out = reinterpret_cast<uint8_t*>(0x25); // PORTB
				bit = _BV(6)|_BV(7)|_BV(5); // D10, D11, D9
				*mode |= bit;
			}

			// reset
			{
				auto pin = kRSTPin;
				auto bit = kPinToBit[pin];
				auto port = kPinToPort[pin];
				auto out = reinterpret_cast<volatile uint8_t*>(kPortToOutput[port]);
				auto timer = kPinToTimer[pin];
				if(timer != NOT_ON_TIMER)
					turnOffPWM(timer);
				*out |= bit; // write HIGH to RST
				delay(1);
				*out &= ~bit; // write LOW to RST
				delay(10);
				*out |= bit; // write HIGH to RST
			}

			// boot lcd
			{
				uint8_t const lcdBoot[] =
				{
					// boot defaults are commented out but left here incase they
					// might prove useful for reference
					// further reading: https://www.adafruit.com/datasheets/SSD1306.pdf
					// display Off
					// 0xAE,
					// set Display Clock Divisor v = 0xF0
					// default is 0x80
					0xD5, 0xF0,
					// set Multiplex Ratio v = 0x3F
					// 0xA8, 0x3F,
					// set Display Offset v = 0
					// 0xD3, 0x00,
					// set Start Line (0)
					// 0x40,
					// charge Pump Setting v = enable (0x14)
					// default is disabled
					0x8D, 0x14,
					// set Segment Re-map (A0) | (b0001)
					// default is (b0000)
					0xA1,
					// set COM Output Scan Direction
					0xC8,
					// set COM Pins v
					// 0xDA, 0x12,
					// set Contrast v = 0xCF
					0x81, 0xCF,
					// set Precharge = 0xF1
					0xD9, 0xF1,
					// set VCom Detect
					// 0xDB, 0x40,
					// entire Display ON
					// 0xA4,
					// set normal/inverse display
					// 0xA6,
					// display On
					0xAF,
					// set display mode = horizontal addressing mode (0x00)
					0x20, 0x00,
					// set col address range
					// 0x21, 0x00, COLUMN_ADDRESS_END,
					// set page address range
					// 0x22, 0x00, PAGE_ADDRESS_END
				};
				spiSetClockDivider(SPI_CLOCK_DIV2);
				_lcdCommandMode();
				for(auto n=uint8_t{0}; n < sizeof(lcdBoot); ++n)
					spiTransfer(lcdBoot[n]);
				_lcdDataMode();
			}

			// save much power
			{
				power_adc_disable();
				power_usart0_disable();
				power_twi_disable();
				// timer 0 is for millis()
				// timers 1 and 3 are for music and sounds
				power_timer2_disable();
				power_usart1_disable();
				// we need USB, for now (to allow triggered reboots to reprogram)
				// power_usb_disable()
			}
		#elif defined(_WIN32) || defined(__linux)
			SDL_CreateWindowAndRenderer(kScreenWidth * 4, kScreenHeight * 4, 0, &_window, &_renderer);
			if((_window == nullptr) || (_renderer == nullptr))
			{
				SDL_Log("failed to init display (\"%s\").\n", SDL_GetError());
				return;
			}
			SDL_RenderSetLogicalSize(_renderer, kScreenWidth, kScreenHeight);
			SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
			SDL_RenderClear(_renderer);
		#endif
		}

		void beginLogo()
		{
			begin();
			bootLogo();
		}

		void bootLogo()
		{
			for(auto y = int8_t{-18}; y<=24; ++y)
			{
				setRGBled(24-y, 0, 0);
				drawBitmap(20, y, arduboy_logo, arduboy_logo_width, arduboy_logo_height);
				displayClear();
				delay(27);
				// longer delay post boot, we put it inside the loop to
				// save the flash calling clear/delay again outside the loop
				if(y == -16)
					delay(250);
			}
			delay(750);
			setRGBled(0,0,0);
		}

		finline uint8_t inputState()
		{
		#if defined(ARDUBOY_10)
			uint8_t state = ((~PINF) & B11110000); // up, right, left, down
			state |= (((~PINE) & B01000000) >> 3); // A
			state |= (((~PINB) & B00010000) >> 2); // B
			return state;
		#elif defined(_WIN32) || defined(__linux__)
			auto sdlEvent = SDL_Event{0};
			while(SDL_PollEvent(&sdlEvent))
			{
				sdlQuit = (sdlEvent.type == SDL_QUIT) || ((sdlEvent.type == SDL_KEYDOWN) && (sdlEvent.key.keysym.scancode == SDL_SCANCODE_ESCAPE));
			}
			auto ks = SDL_GetKeyboardState(nullptr);
			auto state = uint8_t{0};
			state = (ks[SDL_SCANCODE_UP])? kInputUp : 0;
			state |= (ks[SDL_SCANCODE_RIGHT])? kInputRight : 0;
			state |= (ks[SDL_SCANCODE_LEFT])? kInputLeft : 0;
			state |= (ks[SDL_SCANCODE_DOWN])? kInputDown : 0;
			state |= (ks[SDL_SCANCODE_A])? kInputA : 0;
			state |= (ks[SDL_SCANCODE_B])? kInputB : 0;
			return state;
		#endif
		}

		void displayClear()
		{
		#if defined(ARDUBOY_10)
			for(auto n = int16_t{0}; n<kScreenRAMSize; ++n)
			{
				SPDR = _frameBuffer[n];
				_frameBuffer[n] = 0;
				while(!(SPSR & _BV(SPIF))); // wait
			}
		#elif defined(_WIN32) || defined(__linux__)
			auto n = size_t{0};
			for(auto y = int16_t{0}; y < kScreenHeight; y+=8)
			{
				for(auto x = int16_t{0}; x < kScreenWidth; ++x)
				{
					auto col = _frameBuffer[n];
					_frameBuffer[n++] = 0;
					for(auto i = 0; i < 8; ++i)
					{
						auto pixel = col & (1 << i);
						if(pixel == 0)
							SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
						else
							SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
						SDL_RenderDrawPoint(_renderer, x, y+i);
					}
				}
			}
			SDL_SetRenderDrawColor(_renderer, _ledRed, 0, 0, 255);
			SDL_RenderDrawPoint(_renderer, 1, 1);
			SDL_SetRenderDrawColor(_renderer, 0, _ledGreen, 0, 255);
			SDL_RenderDrawPoint(_renderer, 2, 1);
			SDL_SetRenderDrawColor(_renderer, 0, 0, _ledBlue, 255);
			SDL_RenderDrawPoint(_renderer, 3, 1);
			SDL_RenderPresent(_renderer);
		#endif
		}

		void setRGBled(uint8_t red, uint8_t green, uint8_t blue)
		{
		#if defined(ARDUBOY_10)
			// inversion is necessary because these are common annode LEDs
			sbi(TCCR1A, COM1B1);
			OCR1B = 255 - red;
			sbi(TCCR0A, COM0A1);
			OCR0A = 255 - green;
			sbi(TCCR1A, COM1A1);
			OCR1A = 255 - blue;
		#elif defined(_WIN32) || defined(__linux__)
			_ledRed = red;
			_ledGreen = green;
			_ledBlue = blue;
		#endif
		}

		void drawBitmap(int16_t x, int16_t y, uint8_t const* bitmap, uint8_t w, uint8_t h)
		{
			// no need to dar at all of we're offscreen
			if(((x+w) < 0) || (x > (kScreenWidth-1)) || ((y+h) < 0) || (y > (kScreenHeight-1)))
				return;

			auto yOffset = int16_t(abs(y) % 8);
			auto sRow = y / 8;
			if (y < 0)
			{
				--sRow;
				yOffset = 8 - yOffset;
			}
			auto rows = int8_t(h/8);
			if((h%8) != 0)
				++rows;
			for(auto a = int8_t{0}; a < rows; ++a)
			{
				auto bRow = sRow + a;
				if(bRow > ((kScreenHeight/8)-1))
					break;
				if(bRow > -2)
				{
					for (auto iCol = int8_t{0}; iCol<w; ++iCol)
					{
						if((iCol + x) > (kScreenWidth-1))
							break;
						if((iCol + x) >= 0)
						{
							if(bRow >= 0)
								_frameBuffer[ (bRow*kScreenWidth) + x + iCol ] |= pgm_read_byte(bitmap+(a*w)+iCol) << yOffset;
							if((yOffset) && (bRow < ((kScreenHeight/8)-1)) && (bRow > -2))
								_frameBuffer[ ((bRow+1)*kScreenWidth) + x + iCol ] |= pgm_read_byte(bitmap+(a*w)+iCol) >> (8-yOffset);
						}
					}
				}
			}
		}

		void drawPixel(int16_t x, int16_t y, uint8_t color)
		{
			if((x < 0) || (y < 0) || (x >= kScreenWidth) || (y >= kScreenHeight))
				return;
			auto pos = (y / 8) * kScreenWidth + x;
			if(color == eColor::White)
				_frameBuffer[pos] |= _BV(y % 8);
			else
				_frameBuffer[pos] &= ~_BV(y % 8);
		}

		void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint8_t color)
		{
			auto steep = abs(y1 - y0) > abs(x1 - x0);
			if(steep)
			{
				swap(x0, y0);
				swap(x1, y1);
			}
			if(x0 > x1)
			{
				swap(x0, x1);
				swap(y0, y1);
			}

			auto dx = x1 - x0;
			auto dy = abs(y1 - y0);
			auto err = dx >> 1;
			auto ystep = (y0 < y1) ? 1 : -1;
			for(; x0 <= x1; ++x0)
			{
				auto x = (steep) ? y0 : x0;
				auto y = (steep) ? x0 : y0;
				drawPixel(x, y, color);
				err -= dy;
				if(err < 0)
				{
					y0 += ystep;
					err += dx;
				}
			}
		}

		void drawHLine(int16_t x0, int16_t y, int16_t x1, uint8_t color)
		{
			for(auto xpos = x0; xpos <= x1; ++xpos)
				drawPixel(xpos, y, color);
		}

		void drawTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color)
		{
			drawLine(x0, y0, x1, y1, color);
			drawLine(x1, y1, x2, y2, color);
			drawLine(x2, y2, x0, y0, color);
		}

		void fillTriangle(int16_t x0, int16_t y0, int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint8_t color)
		{
			int16_t a, b, y, last;
			// sort coordinates by y order (y2 >= y1 >= y0)
			if(y0 > y1)
			{
				swap(y0, y1);
				swap(x0, x1);
			}
			if(y1 > y2)
			{
				swap(y2, y1);
				swap(x2, x1);
			}
			if(y0 > y1)
			{
				swap(y0, y1);
				swap(x0, x1);
			}
			// handle awkward all-on-same-line case as its own thing
			if(y0 == y2)
			{
				a = b = x0;
				if(x1 < a)
					a = x1;
				else if(x1 > b)
					b = x1;
				if(x2 < a)
					a = x2;
				else if(x2 > b)
					b = x2;
				drawHLine(a, y0, b, color);
				return;
			}
			auto dx01 = x1 - x0;
			auto dy01 = y1 - y0;
			auto dx02 = x2 - x0;
			auto dy02 = y2 - y0;
			auto dx12 = x2 - x1;
			auto dy12 = y2 - y1;
			auto sa = int32_t{0};
			auto sb = int32_t{0};
			// for upper part of triangle, find scanline crossings for segments
			// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
			// is included here (and second loop will be skipped, avoiding a /0
			// error there), otherwise scanline y1 is skipped here and handled
			// in the second loop...which also avoids a /0 error here if y0=y1
			// (flat-topped triangle).
			if(y1 == y2)
				last = y1; // include y1 scanline
			else
				last = y1-1; // skip it
			for(y=y0; y<=last; ++y)
			{
				a = x0 + sa / dy01;
				b = x0 + sb / dy02;
				sa += dx01;
				sb += dx02;
				// longhand:
				//a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
				//b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
				if(a > b)
					swap(a, b);
				drawHLine(a, y, b, color);
			}
			// for lower part of triangle, find scanline crossings for segments
			// 0-2 and 1-2.  This loop is skipped if y1=y2.
			sa = dx12 * (y - y1);
			sb = dx02 * (y - y0);
			for(; y<=y2; ++y)
			{
				a = x1 + sa / dy12;
				b = x0 + sb / dy02;
				sa += dx12;
				sb += dx02;
				// longhand:
				//a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
				//b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
				if(a > b)
					swap(a,b);
				drawHLine(a, y, b, color);
			}
		}

	private:
		uint8_t _frameBuffer[kScreenRAMSize];

	#if defined(_WIN32) || defined(__linux__)
	private:
		SDL_Window* _window{nullptr};
		SDL_Renderer* _renderer{nullptr};
		uint8_t _ledRed{0};
		uint8_t _ledGreen{0};
		uint8_t _ledBlue{0};
	#endif
	};
}
using ZArduboy = z::Arduboy;
