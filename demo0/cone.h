#pragma once
#include "zfw.h"

z::FPType constexpr cone_vb[] PROGMEM =
{
	z::fp(0.0f), z::fp(1.2f), z::fp(-1.2f),
	z::fp(0.848528f), z::fp(0.848528f), z::fp(-1.2f),
	z::fp(1.2f), z::fp(-0.0f), z::fp(-1.2f),
	z::fp(0.848528f), z::fp(-0.848528f), z::fp(-1.2f),
	z::fp(-0.0f), z::fp(-1.2f), z::fp(-1.2f),
	z::fp(0.0f), z::fp(0.0f), z::fp(1.2f),
	z::fp(-0.848528f), z::fp(-0.848528f), z::fp(-1.2f),
	z::fp(-1.2f), z::fp(0.0f), z::fp(-1.2f),
	z::fp(-0.848528f), z::fp(0.848528f), z::fp(-1.2f),
};
auto constexpr cone_vbSize = uint16_t{9};

z::FPType constexpr cone_vtb[] PROGMEM =
{
	z::fp(0.25f), z::fp(0.49f),
	z::fp(0.25f), z::fp(0.25f),
	z::fp(0.4197f), z::fp(0.4197f),
	z::fp(0.49f), z::fp(0.25f),
	z::fp(0.4197f), z::fp(0.0803f),
	z::fp(0.25f), z::fp(0.01f),
	z::fp(0.0803f), z::fp(0.0803f),
	z::fp(0.01f), z::fp(0.25f),
	z::fp(0.0803f), z::fp(0.4197f),
	z::fp(0.9197f), z::fp(0.0803f),
	z::fp(0.5803f), z::fp(0.0803f),
	z::fp(0.9197f), z::fp(0.4197f),
	z::fp(0.5803f), z::fp(0.4197f),
	z::fp(0.75f), z::fp(0.49f),
	z::fp(0.99f), z::fp(0.25f),
	z::fp(0.75f), z::fp(0.01f),
	z::fp(0.51f), z::fp(0.25f),
};
auto constexpr cone_vtbSize = uint16_t{17};

z::FPType constexpr cone_vnb[] PROGMEM =
{
	z::fp(0.3474f), z::fp(0.8387f), z::fp(0.4194f),
	z::fp(0.8387f), z::fp(0.3474f), z::fp(0.4194f),
	z::fp(0.8387f), z::fp(-0.3474f), z::fp(0.4194f),
	z::fp(0.3474f), z::fp(-0.8387f), z::fp(0.4194f),
	z::fp(-0.3474f), z::fp(-0.8387f), z::fp(0.4194f),
	z::fp(-0.8387f), z::fp(-0.3474f), z::fp(0.4194f),
	z::fp(-0.8387f), z::fp(0.3474f), z::fp(0.4194f),
	z::fp(-0.3474f), z::fp(0.8387f), z::fp(0.4194f),
	z::fp(0.0f), z::fp(0.0f), z::fp(-1.0f),
};
auto constexpr cone_vnbSize = uint16_t{9};

uint16_t constexpr cone_ib[] PROGMEM =
{
	0, 0, 0,
	5, 1, 0,
	1, 2, 0,
	1, 2, 1,
	5, 1, 1,
	2, 3, 1,
	2, 3, 2,
	5, 1, 2,
	3, 4, 2,
	3, 4, 3,
	5, 1, 3,
	4, 5, 3,
	4, 5, 4,
	5, 1, 4,
	6, 6, 4,
	6, 6, 5,
	5, 1, 5,
	7, 7, 5,
	7, 7, 6,
	5, 1, 6,
	8, 8, 6,
	8, 8, 7,
	5, 1, 7,
	0, 0, 7,
	3, 9, 8,
	6, 10, 8,
	1, 11, 8,
	8, 12, 8,
	0, 13, 8,
	1, 11, 8,
	1, 11, 8,
	2, 14, 8,
	3, 9, 8,
	3, 9, 8,
	4, 15, 8,
	6, 10, 8,
	6, 10, 8,
	7, 16, 8,
	8, 12, 8,
	8, 12, 8,
	1, 11, 8,
	6, 10, 8,
};
auto constexpr cone_ibSize = uint16_t{42};

