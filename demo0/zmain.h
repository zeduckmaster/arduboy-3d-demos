#pragma once
#include "zarduboy.h"

ZArduboy zarduboy;

#include "cube.h"
#include "cone.h"
#include "ico.h"
#include "teapot.h"

namespace z
{
	struct Model
	{
		FPType const* vb;
		uint16_t vbSize;
		uint16_t const* ib;
		uint16_t ibSize;
	};

	Model constexpr models[] =
	{
		{ cube_vb, cube_vbSize, cube_ib, cube_ibSize },
		{ cone_vb, cone_vbSize, cone_ib, cone_ibSize },
		{ ico_vb, ico_vbSize, ico_ib, ico_ibSize },
		{ teapot_vb, teapot_vbSize, teapot_ib, teapot_ibSize },
	};
	auto constexpr kNbModel = uint8_t{4};
	uint8_t curModel;
	FPType const* vb;
	uint16_t vbSize;
	uint16_t const* ib;
	uint16_t ibSize;

	void setModel(uint8_t modelIndex)
	{
		auto& model = models[modelIndex];
		vb = model.vb;
		vbSize = model.vbSize;
		ib = model.ib;
		ibSize = model.ibSize;
	}

	enum eRendering : uint8_t
	{
		Wireframe = 0,
		WireframeCulled,
		Shade,
		ShadeWireframe,

		NbRendering
	};

	uint8_t previousInputState;
	eRendering rendering;
	eColor lineColor;

	////////////////////////////////////////////////////////////////////////////
	// math stuff

	struct Pt
	{
		int16_t x, y;
	};

	Pt screenPts[137]; // enough to store all models
	int16_t rotx;
	int16_t rotz;

	// mv: pos(0,-3.5,0) at(0,0,0) up(0,0,1)
	// mp: fovh(90) ar(128/64) near(0.1) far(10)
	Mat4 matProjView
	{
		fp(1.0f),	0,			0,				0, // col0
		0,			0,			fp(1.02020f),	fp(1.0f), // col1
		0,			fp(2.0f),	0,				0, // col2
		0,			0,			fp(3.87879f),	fp(4.0f) // col3
	};

	Mat4 matWorld;

	void worldToScreen(Pt& dst, Vec3 const& v)
	{
		// matProjView * Vec4
		Mat4 mx, mz, mm;
		matRotX(mx, rotx);
		matRotZ(mz, rotz);
		mul(matWorld, mx, mz);
		mul(mm, matProjView, matWorld);
		Vec4 clipPt;
		mul(clipPt, mm, Vec4{v.x, v.y, v.z, fp(1.0f)});
		auto x = fpDiv(clipPt.x, clipPt.w);
		auto y = fpDiv(clipPt.y, clipPt.w);
		x = fpMul(x + fp(1.0f), fp(0.5f));
		y = fpMul(-y + fp(1.0f), fp(0.5f));
		dst.x = (uint32_t(x & detail::kFPFracMask) * zarduboy.width()) / detail::kFPFracMask;
		dst.y = (uint32_t(y & detail::kFPFracMask) * zarduboy.height()) / detail::kFPFracMask;
	}

	inline bool isCCW(Pt const& p0, Pt const& p1, Pt const& p2)
	{
		auto xa = p1.x - p0.x;
		auto ya = p1.y - p0.y;
		auto xb = p2.x - p0.x;
		auto yb = p2.y - p0.y;
		auto t = (xa*yb) - (ya*xb);
		return t < 0;
	}
	
	////////////////////////////////////////////////////////////////////////////
	// main program

	void setup()
	{
		zarduboy.begin();
		zarduboy.bootLogo();

		rotx = 0;
		rotz = 0;
		curModel = 0;
		setModel(curModel);

		previousInputState = 0;
		rendering = eRendering::Wireframe;
		lineColor = eColor::White;
	}

	void loop()
	{
		// update input
		{
			auto state = zarduboy.inputState();
			if(state & zarduboy.kInputUp)
				--rotx;
			if(state & zarduboy.kInputDown)
				++rotx;

			if(state & zarduboy.kInputRight)
				++rotz;
			if(state & zarduboy.kInputLeft)
				--rotz;
			if((previousInputState & zarduboy.kInputA)
			&& !(state & zarduboy.kInputA))
			{
				++curModel;
				if(curModel >= kNbModel)
					curModel = 0;
				setModel(curModel);
			}

			if((previousInputState & zarduboy.kInputB)
			&& !(state & zarduboy.kInputB))
			{
				rendering = static_cast<eRendering>(rendering + 1);
				if(rendering == eRendering::NbRendering)
					rendering = eRendering::Wireframe;

				if(rendering >= eRendering::Shade)
					lineColor = eColor::Black;
				else
					lineColor = eColor::White;
			}

			if(rotz < 0)
				rotz = 359;
			if(rotz >= 360)
				rotz = 0;
			if(rotx < 0)
				rotx = 359;
			if(rotx >= 360)
				rotx = 0;
			previousInputState = state;
		}

		// draw
		{
			// run vertex shader
			for(auto n = uint16_t{0}; n < vbSize; ++n)
			{
				auto v = Vec3
				{
					static_cast<int16_t>(pgm_read_word(vb+n*3)),
					static_cast<int16_t>(pgm_read_word(vb+n*3+1)),
					static_cast<int16_t>(pgm_read_word(vb+n*3+2))
				};
				worldToScreen(screenPts[n], v);
			}

			// run pixel shader
			for(auto n = uint16_t{0}; n < ibSize; n+=3)
			{
				auto vtx0 = pgm_read_word(ib+n*3);
				auto vtx1 = pgm_read_word(ib+(n+1)*3);
				auto vtx2 = pgm_read_word(ib+(n+2)*3);
				auto& p0 = screenPts[vtx0];
				auto& p1 = screenPts[vtx1];
				auto& p2 = screenPts[vtx2];

				if((rendering != eRendering::Wireframe) && (isCCW(p0, p1, p2) == false))
					continue;
				if(rendering >= eRendering::Shade)
					zarduboy.fillTriangle(p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, eColor::White);
				if(rendering != eRendering::Shade)
				{
					zarduboy.drawLine(p0.x, p0.y, p1.x, p1.y, lineColor);
					zarduboy.drawLine(p1.x, p1.y, p2.x, p2.y, lineColor);
					zarduboy.drawLine(p2.x, p2.y, p0.x, p0.y, lineColor);
				}
			}
		}
		zarduboy.displayClear();
	}
}
