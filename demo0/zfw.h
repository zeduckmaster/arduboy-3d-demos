#pragma once

#define FP_5_11

#include <stdint.h>

#if defined(ARDUINO_ARCH_AVR)

#define finline __attribute__((always_inline)) inline

#elif defined(_WIN32) || defined(__linux__)

#include <cstring>

#define finline inline
#define PROGMEM
#define _BV(bit) (1<<(bit))

inline uint8_t pgm_read_byte(uint8_t const* p) { return *p; }
template<typename T>
inline uint16_t pgm_read_word(T const* p) { return *reinterpret_cast<const uint16_t*>(p); }

#endif

namespace z
{
	template<typename T>
	finline void swap(T& a, T& b) { auto t = a; a = b; b = t; }

	///////////////////////////////////////////////////////////////////////////
	// fixed point
	
#if defined(FP_4_12) // [-8, 7.99975] : 0.00025

	#define FP 1
	using FPType = int16_t;
	namespace detail
	{
		using FPTypeNext = int32_t;
		auto constexpr kFPIntBits = uint8_t{4};
		auto constexpr kFPFracBits = uint8_t{12};
	}

#elif defined(FP_5_11) // [-16, 15.99951] : 0.00049

	#define FP 1
	using FPType = int16_t;
	namespace detail
	{
		using FPTypeNext = int32_t;
		auto constexpr kFPIntBits = uint8_t{5};
		auto constexpr kFPFracBits = uint8_t{11};
	}

#elif defined(FP_6_10) // [-32, 31.99902] : 0.00098

	#define FP 1
	using FPType = int16_t;
	namespace detail
	{
		using FPTypeNext = int32_t;
		auto constexpr kFPIntBits = uint8_t{6};
		auto constexpr kFPFracBits = uint8_t{10};
	}

#elif defined(FP_8_8) // [-128, 127.99609] : 0.00391

	#define FP 1
	using FPType = int16_t;
	namespace detail
	{
		using FPTypeNext = int32_t;
		auto constexpr kFPIntBits = uint8_t{8};
		auto constexpr kFPFracBits = uint8_t{8};
	}

#endif

#if defined(FP)

	namespace detail
	{
		auto constexpr kFPFracMask = uint16_t(~((~FPType{0}) << kFPFracBits));
		auto constexpr kFPIntMask = uint16_t(~kFPFracMask);
		auto constexpr kFPOne = FPType{1 << kFPFracBits};
	}

	constexpr FPType fp(float value)
	{
		return static_cast<FPType>(value * detail::kFPOne);
	}

	constexpr FPType fp(int value)
	{
		return static_cast<FPType>(value) << detail::kFPFracBits;
	}

	finline float fpToFloat(FPType value)
	{
		return static_cast<float>(value) / detail::kFPOne;
	}

	finline int fpToInt(FPType value)
	{
		return (value & detail::kFPIntMask) >> detail::kFPFracBits;
	}

	finline FPType fpMul(FPType a, FPType b)
	{
		auto tmp = static_cast<detail::FPTypeNext>(a) * static_cast<detail::FPTypeNext>(b);
		return tmp >> detail::kFPFracBits;
	}

	finline FPType fpDiv(FPType a, FPType b)
	{
		auto tmp = static_cast<detail::FPTypeNext>(a) << detail::kFPFracBits;
		return static_cast<detail::FPTypeNext>(tmp / b);
	}

#endif

	///////////////////////////////////////////////////////////////////////////
	// math

#if defined(FP)

	// from 0 to 90 degrees
	FPType constexpr kCosLut[] PROGMEM =
	{
		fp(1.00000f), fp(0.99985f), fp(0.99939f), fp(0.99863f), fp(0.99756f),
		fp(0.99619f), fp(0.99452f), fp(0.99255f), fp(0.99027f), fp(0.98769f),
		fp(0.98481f), fp(0.98163f), fp(0.97815f), fp(0.97437f), fp(0.97030f),
		fp(0.96593f), fp(0.96126f), fp(0.95630f), fp(0.95106f), fp(0.94552f),
		fp(0.93969f), fp(0.93358f), fp(0.92718f), fp(0.92050f), fp(0.91355f),
		fp(0.90631f), fp(0.89879f), fp(0.89101f), fp(0.88295f), fp(0.87462f),
		fp(0.86603f), fp(0.85717f), fp(0.84805f), fp(0.83867f), fp(0.82904f),
		fp(0.81915f), fp(0.80902f), fp(0.79864f), fp(0.78801f), fp(0.77715f),
		fp(0.76604f), fp(0.75471f), fp(0.74314f), fp(0.73135f), fp(0.71934f),
		fp(0.70711f), fp(0.69466f), fp(0.68200f), fp(0.66913f), fp(0.65606f),
		fp(0.64279f), fp(0.62932f), fp(0.61566f), fp(0.60182f), fp(0.58779f),
		fp(0.57358f), fp(0.55919f), fp(0.54464f), fp(0.52992f), fp(0.51504f),
		fp(0.50000f), fp(0.48481f), fp(0.46947f), fp(0.45399f), fp(0.43837f),
		fp(0.42262f), fp(0.40674f), fp(0.39073f), fp(0.37461f), fp(0.35837f),
		fp(0.34202f), fp(0.32557f), fp(0.30902f), fp(0.29237f), fp(0.27564f),
		fp(0.25882f), fp(0.24192f), fp(0.22495f), fp(0.20791f), fp(0.19081f),
		fp(0.17365f), fp(0.15643f), fp(0.13917f), fp(0.12187f), fp(0.10453f),
		fp(0.08716f), fp(0.06976f), fp(0.05233f), fp(0.03490f), fp(0.01745f),
		fp(0.0f)
	};

	finline FPType readCosLut(uint16_t i)
	{
		return pgm_read_word(&kCosLut[i]);
	}
	
	FPType sinFast(uint16_t angle)
	{
		angle += 90;
		if(angle > 450)
			return readCosLut(0);
		if((angle > 360) && (angle < 451))
			return -readCosLut(angle - 360);
		if((angle > 270) && (angle < 361))
			return -readCosLut(360 - angle);
		if((angle > 180) && (angle < 271))
			return  readCosLut(angle - 180);
		return readCosLut(180 - angle);
	}
	
	FPType cosFast(uint16_t angle)
	{
		if(angle > 360)
			return readCosLut(0);
		if((angle > 270) && (angle < 361))
			return  readCosLut(360 - angle);
		if((angle > 180) && (angle < 271))
			return -readCosLut(angle - 180);
		if((angle > 90) && (angle < 181))
			return -readCosLut(180 - angle);
		return readCosLut(angle);
	}

	struct Vec4
	{
		FPType x, y, z, w;
	};

	struct Vec3
	{
		FPType x, y, z;
	};

	struct Vec2
	{
		FPType x, y;
	};

	// index based vertex data
	struct IVtx
	{
		uint16_t ipos;
		uint16_t iuv;
		uint16_t inm;
	};

	//struct Mat3
	//{
	//	FPType m0, m1, m2;
	//	FPType m3, m4, m5;
	//	FPType m6, m7, m8;
	//};
	//
	//struct Mat4
	//{
	//	FPType m0, m1, m2, m3;
	//	FPType m4, m5, m6, m7;
	//	FPType m8, m9, m10, m11;
	//	FPType m12, m13, m14, m15;
	//};

	using Mat3 = FPType[9];
	using Mat4 = FPType[16];

	// 0 4 8  12
	// 1 5 9  13
	// 2 6 10 14
	// 3 7 11 15
	inline void mul(Vec4& dst, Mat4 const& mat, Vec4 const& vec)
	{
		dst.x = fpMul(mat[0], vec.x) + fpMul(mat[4], vec.y) + fpMul(mat[8], vec.z) + fpMul(mat[12], vec.w);
		dst.y = fpMul(mat[1], vec.x) + fpMul(mat[5], vec.y) + fpMul(mat[9], vec.z) + fpMul(mat[13], vec.w);
		dst.z = fpMul(mat[2], vec.x) + fpMul(mat[6], vec.y) + fpMul(mat[10], vec.z) + fpMul(mat[14], vec.w);
		dst.w = fpMul(mat[3], vec.x) + fpMul(mat[7], vec.y) + fpMul(mat[11], vec.z) + fpMul(mat[15], vec.w);
	}

	inline void mul(Vec3& dst, Mat3 const& mat, Vec3 const& vec)
	{
		dst.x = fpMul(mat[0], vec.x) + fpMul(mat[3], vec.y) + fpMul(mat[6], vec.z);
		dst.y = fpMul(mat[1], vec.x) + fpMul(mat[4], vec.y) + fpMul(mat[7], vec.z);
		dst.z = fpMul(mat[2], vec.x) + fpMul(mat[5], vec.y) + fpMul(mat[8], vec.z);
	}

	inline void mul(Mat4& dst, Mat4 const& mat0, Mat4 const& mat1)
	{
		FPType* d{dst};
		FPType const* m1{mat1};
		for(auto r = uint8_t{0}; r < 4; ++r)
		{
			FPType const* m0{mat0};
			for(auto c = uint8_t{0}; c < 4; ++c)
			{
				*d = fpMul(m0[0], m1[0]) + fpMul(m0[4], m1[1]) +
					fpMul(m0[8], m1[2]) + fpMul(m0[12], m1[3]);
				++d;
				++m0;
			}
			m1+=4;
		}
	}

	inline void matRotX(Mat4& dst, int16_t degrees)
	{
		memset(dst, 0, sizeof(FPType)*16);
		dst[0] = dst[15] = fp(1.0f);
		dst[5] = dst[10] = cosFast(degrees);
		dst[6] = sinFast(degrees);
		dst[9] = -dst[6];
	}
	
	inline void matRotY(Mat4& dst, int16_t degrees)
	{
		memset(dst, 0, sizeof(FPType)*16);
		dst[5] = dst[15] = fp(1.0f);
		dst[0] = dst[10] = cosFast(degrees);
		dst[8] = sinFast(degrees);
		dst[2] = -dst[8];
	}
	
	inline void matRotZ(Mat4& dst, int16_t degrees)
	{
		memset(dst, 0, sizeof(FPType)*16);
		dst[10] = dst[15] = fp(1.0f);
		dst[0] = dst[5] = cosFast(degrees);
		dst[1] = sinFast(degrees);
		dst[4] = -dst[1];
	}

#endif

}
