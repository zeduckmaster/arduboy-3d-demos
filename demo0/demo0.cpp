#if defined(_WIN32) || defined(__linux__)
#include "zmain.h"
#if defined(_WIN32)
#include <SDL.h>
#elif defined(__linux__)
#include <SDL2/SDL.h>
#endif

#if defined(__cplusplus)
extern "C"
#endif
int main(int argc, char* argv[])
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		SDL_Log("Failed to initialize SDL (\"%s\").\n", SDL_GetError());
		return -1;
	}
	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, 0);

	z::setup();
	while(sdlQuit == false)
	{
		z::loop();
	}
	return 0;
}
#endif
