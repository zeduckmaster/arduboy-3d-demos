#pragma once
#include "zfw.h"

z::FPType constexpr cube_vb[] PROGMEM =
{
	z::fp(-1.0f), z::fp(1.0f), z::fp(1.0f),
	z::fp(-1.0f), z::fp(1.0f), z::fp(-1.0f),
	z::fp(-1.0f), z::fp(-1.0f), z::fp(-1.0f),
	z::fp(1.0f), z::fp(1.0f), z::fp(1.0f),
	z::fp(1.0f), z::fp(1.0f), z::fp(-1.0f),
	z::fp(1.0f), z::fp(-1.0f), z::fp(1.0f),
	z::fp(1.0f), z::fp(-1.0f), z::fp(-1.0f),
	z::fp(-1.0f), z::fp(-1.0f), z::fp(1.0f),
};
auto constexpr cube_vbSize = uint16_t{8};

z::FPType constexpr cube_vtb[] PROGMEM =
{
	z::fp(0.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(1.0f),
	z::fp(1.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(0.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(1.0f),
	z::fp(1.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(0.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(0.0f), z::fp(1.0f),
	z::fp(1.0f), z::fp(0.0f),
};
auto constexpr cube_vtbSize = uint16_t{20};

z::FPType constexpr cube_vnb[] PROGMEM =
{
	z::fp(-1.0f), z::fp(-0.0f), z::fp(0.0f),
	z::fp(0.0f), z::fp(1.0f), z::fp(0.0f),
	z::fp(1.0f), z::fp(-0.0f), z::fp(0.0f),
	z::fp(0.0f), z::fp(-1.0f), z::fp(-0.0f),
	z::fp(0.0f), z::fp(0.0f), z::fp(-1.0f),
	z::fp(0.0f), z::fp(-0.0f), z::fp(1.0f),
};
auto constexpr cube_vnbSize = uint16_t{6};

uint16_t constexpr cube_ib[] PROGMEM =
{
	0, 0, 0,
	1, 1, 0,
	2, 2, 0,
	3, 3, 1,
	4, 4, 1,
	1, 5, 1,
	5, 6, 2,
	6, 7, 2,
	4, 4, 2,
	7, 8, 3,
	2, 2, 3,
	6, 9, 3,
	1, 10, 4,
	4, 11, 4,
	6, 12, 4,
	3, 13, 5,
	0, 14, 5,
	7, 15, 5,
	7, 16, 0,
	0, 0, 0,
	2, 2, 0,
	0, 0, 1,
	3, 3, 1,
	1, 5, 1,
	3, 17, 2,
	5, 6, 2,
	4, 4, 2,
	5, 6, 3,
	7, 8, 3,
	6, 9, 3,
	2, 18, 4,
	1, 10, 4,
	6, 12, 4,
	5, 19, 5,
	3, 13, 5,
	7, 15, 5,
};
auto constexpr cube_ibSize = uint16_t{36};

