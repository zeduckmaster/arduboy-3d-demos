Some small 3D experiments/demos on Arduboy.

# Installation

## Arduboy

Each demo has been compiled with official Arduino ide 1.6.12 and tested on Arduboy v1.

## Desktop

If you don't have an Arduboy you can build and test the demos on desktop using the Qt Creator project for each demo.

The controls are the same as the Arduboy, plus you can quit using *Escape* key.

# Demo0

![alt text](https://framagit.org/zeduckmaster/arduboy-3d-demos/raw/master/screens/demo0.png "Demo0 screenshot")

More a proof of concept than a real demo.

Program size: 15,354 bytes (53%) on 28,672 bytes.

Dynamic memory: 1,844 bytes (72%) on 2,560 bytes.

3D models:

* cube: 8 vertices, 12 triangles.
* cone: 9 vertices, 14 triangles.
* icosahedron: 12 vertices, 20 triangles.
* teapot: 137 vertices, 240 triangles.

Rendering type:

* wireframe.
* wireframe with backface culling.
* fill with backface culling.
* fill and wireframe with backface culling.

Controls:

* *up*, *down*, *left*, *right* : rotate the model.
* *A* : switch between 3D models.
* *B* : switch between rendering type.

# Information

These demos use ZArduboy which is a stripped and optimized version of the official Arduboy and Arduino libraries.

This library was not intended to be used outside these demos so it lacks some features of the original libaries.

Moreover, most of pin, serial (spi) and boot codes has been inlined and heavily optimized in regards of the specifications of the Arduboy v1.

My initial motivation was inspired by the work of mrt-prodz on his [Tiny 3D Engine](https://github.com/mrt-prodz/ATmega328-Tiny-3D-Engine) for arduino.

The fixed point code, which all 3D computation is based on, is a derivative of [eteran fixed point math](https://github.com/eteran/cpp-utilities), but stripped and optimized for Arduboy.

# Related links

* Arduboy library: https://github.com/Arduboy/Arduboy
* TEAM-Arg: https://github.com/TEAMarg
* mrt-prodz Tiny 3D Engine: https://github.com/mrt-prodz/ATmega328-Tiny-3D-Engine
* eteran fixed point math: https://github.com/eteran/cpp-utilities


License: GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html