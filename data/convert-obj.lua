----------------------------------------------------------------------
-- input data

local objname = "teapot"
local srcfilename = objname..".obj"
local dstfilename = "../demo0/"..objname..".h"

----------------------------------------------------------------------
-- actual code

local vb = {}
local vtb = {}
local vnb = {}
local ib = {}
local vbSize = 1
local vtbSize = 1
local vnbSize = 1
local ibSize = 1
for l in io.lines(srcfilename) do
	local c = l:sub(1, 2)
	if c == "v " then
		--print(l)
		for k, l, m in string.gmatch(l, "v (%-*%w+%.*%w*) (%-*%w+%.*%w*) (%-*%w+%.*%w*)") do
			local vec = {}
			vec.x = tonumber(k)
			vec.y = tonumber(l)
			vec.z = tonumber(m)
			vb[vbSize] = vec
			vbSize = vbSize + 1
			--print("("..pos.x..", "..pos.y..", "..pos.z..")")
		end
	elseif c == "vt" then
		for k, l in string.gmatch(l, "vt (%-*%w+%.*%w*) (%-*%w+%.*%w*)") do
			local vec= {}
			vec.x = tonumber(k)
			vec.y = tonumber(l)
			vtb[vtbSize] = vec
			vtbSize = vtbSize + 1
			--print("("..pos.x..", "..pos.y..", "..pos.z..")")
		end
	elseif c == "vn" then
		for k, l, m in string.gmatch(l, "vn (%-*%w+%.*%w*) (%-*%w+%.*%w*) (%-*%w+%.*%w*)") do
			local vec= {}
			vec.x = tonumber(k)
			vec.y = tonumber(l)
			vec.z = tonumber(m)
			vnb[vnbSize] = vec
			vnbSize = vnbSize + 1
			--print("("..pos.x..", "..pos.y..", "..pos.z..")")
		end
	elseif c == "f " then
		--print(l)
		for fpos0, fuv0, fnm0, fpos1, fuv1, fnm1, fpos2, fuv2, fnm2
		in string.gmatch(l, "f (%w+)/(%w*)/(%w+) (%w+)/(%w*)/(%w+) (%w+)/(%w*)/(%w+)") do
			local vtx = {}
			vtx.pos = tonumber(fpos0)-1
			vtx.uv = tonumber(fuv0)-1
			vtx.nm = tonumber(fnm0)-1
			ib[ibSize] = vtx
			ibSize = ibSize + 1
			
			vtx = {}
			vtx.pos = tonumber(fpos1)-1
			vtx.uv = tonumber(fuv1)-1
			vtx.nm = tonumber(fnm1)-1
			ib[ibSize] = vtx
			ibSize = ibSize + 1
			
			vtx = {}
			vtx.pos = tonumber(fpos2)-1
			vtx.uv = tonumber(fuv2)-1
			vtx.nm = tonumber(fnm2)-1
			ib[ibSize] = vtx
			ibSize = ibSize + 1
		end
	end
end

local file = io.open(dstfilename, "w")
file:write("#pragma once\n#include \"zfw.h\"\n\n")

file:write("z::FPType constexpr "..objname.."_vb[] PROGMEM =\n{\n")
for k, vec in pairs(vb) do
	file:write("\tz::fp("..tostring(vec.x).."f), z::fp("..tostring(vec.y).."f), z::fp("..tostring(vec.z).."f),\n")
end
file:write("};\n")
file:write("auto constexpr "..objname.."_vbSize = uint16_t{"..#vb.."};\n\n")

file:write("z::FPType constexpr "..objname.."_vtb[] PROGMEM =\n{\n")
for k, vec in pairs(vtb) do
	file:write("\tz::fp("..tostring(vec.x).."f), z::fp("..tostring(vec.y).."f),\n")
end
file:write("};\n")
file:write("auto constexpr "..objname.."_vtbSize = uint16_t{"..#vtb.."};\n\n")

file:write("z::FPType constexpr "..objname.."_vnb[] PROGMEM =\n{\n")
for k, vec in pairs(vnb) do
	file:write("\tz::fp("..tostring(vec.x).."f), z::fp("..tostring(vec.y).."f), z::fp("..tostring(vec.z).."f),\n")
end
file:write("};\n")
file:write("auto constexpr "..objname.."_vnbSize = uint16_t{"..#vnb.."};\n\n")

file:write("uint16_t constexpr "..objname.."_ib[] PROGMEM =\n{\n")
for k, vtx in pairs(ib) do
	file:write("\t"..tostring(vtx.pos)..", "..tostring(vtx.uv)..", "..tostring(vtx.nm)..",\n")
end
file:write("};\n")
file:write("auto constexpr "..objname.."_ibSize = uint16_t{"..(#ib).."};\n\n")
--file:write("\tz::Pt rst[] =\n\t{\n")
--for n = 0, #vb, 1 do
--	file:write("\t\t{0}, \n")
--end
--file:write("\t};\n")

--file:write("}\n")

io.close(file)

--print(#vb)
--print(#ib)