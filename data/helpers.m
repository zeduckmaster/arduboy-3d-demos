function ret = computeProjFOVH(fovhRad, aspectRatio, znear, zfar)
  fright = znear * tan(fovhRad/2);
  fleft = -fright;
  ftop = fright / aspectRatio;
  fbottom = -ftop;
  
  rml = fright - fleft;
  rpl = fright + fleft;
  tmb = ftop - fbottom;
  tpb = ftop + fbottom;
  invnmf = 1.0 / (znear - zfar);
  
  ret = [ (2.0*znear/rml), 0, (rpl/rml), 0;
          0, (2.0*znear/tmb), (tpb/tmb), 0;
          0, 0, ((zfar+znear)*invnmf), (2.0*zfar*znear*invnmf);
          0, 0, -1.0, 0 ];
endfunction

function ret = normalize(v)
  ret = v / norm(v)
 endfunction

function ret = computeViewLookDir(pos, dir, up)
  tdir = normalize(-dir);
  tright = normalize(cross(up, tdir));
  tup = normalize(cross(tdir, tright));
  rot = [ tright(1), tup(1), tdir(1), 0;
          tright(2), tup(2), tdir(2), 0;
          tright(3), tup(3), tdir(3), 0;
          0, 0, 0, 1 ];
  tr = [ 1, 0, 0, pos(1);
         0, 1, 0, pos(2);
         0, 0, 1, pos(3);
         0, 0, 0, 1 ];
  mw = tr * rot;
  ret = zeros(4, 4);
  ret(1) = mw(1);
  ret(2) = mw(5);
  ret(3) = mw(9);
  ret(4) = 0;
  ret(5) = mw(2);
  ret(6) = mw(6);
  ret(7) = mw(10);
  ret(8) = 0;
  ret(9) = mw(3);
  ret(10) = mw(7);
  ret(11) = mw(11);
  ret(12) = 0;
  ret(13) = -(mw(1)*mw(13) + mw(2)*mw(14) + mw(3)*mw(15));
  ret(14) = -(mw(5)*mw(13) + mw(6)*mw(14) + mw(7)*mw(15));
  ret(15) = -(mw(9)*mw(13) + mw(10)*mw(14) + mw(11)*mw(15));
  ret(16) = 1;
endfunction

function ret = computeViewLookAt(pos, at, up)
  ret = computeViewLookDir(pos, at-pos, up)
endfunction